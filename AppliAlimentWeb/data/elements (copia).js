var output = "</span><span class=\"fa fa-star stars-checked\"></span><span class=\"fa fa-star stars-checked\"></span><span class=\"fa fa-star stars-checked\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span>"
var elements = {
  "ontology":"gogofull",
  "data":[
    {
      "id":"1DM",
      "status":1,
      "moderationState":0,
      "name":"Biotope de Kergouaran",
      "geo":{
        "latitude":47.8088,
        "longitude":-3.3425
      },
      "address":{
        "streetAddress":"<b>Addresse:</b> 82 rue Poincarré ZA de Kergouaran",
        "addressLocality":"Caudan",
        "postalCode":"56850",
        "addressCountry":"FR"
      },
      "nom":"<b>Nom: </b> Dominique Morantin",
      "telephone":"<b>Telephone</b>: 06 77 05 81 95",
      "openHoursMoreInfos":"<b>Horaires:</b> sur rendez-vous de 11h à 18h le jeudi, vendredi et samedi",      
      "website":"<b>Lien</b>: -",   
      "Liste Produits":"<b>Liste Produits</b>: Tomate, salade, carotte, artichaut",
      "avis":"<b>Avis: </b>"+ output +" \"Tomates delicieux\" - Claude Dubois.",
      "modeAcheminement":"<b>Mode d'acheminement: </b> Vélo, voiture.",
      "conditionElevage":"<b>Condition d'elevage: </b> Exterieure.",
      "circuitProduction":"<b>Circuit de production: </b> Court, directe, cooperative.",   
      "modeEmballageProduit":"<b>Mode emballage de produit: </b> Barquette, cagette .",
      "histoire":"<b>Histoire: </b> En 1983, Dominique s’installe comme maraîcher à Caudan. N’ayant jamais traité ses terres, il décide de convertir sa ferme en bio en 1990. Depuis, il cultive plusieurs espèces de légumes bio, essentiellement l’été. Avec la volonté de faire découvrir la diversité des légumes qui existent, Dominique cultive toujours plusieurs variétés. Ainsi, il vous propose chaque année près de 15 variétés de tomates, beaucoup de salades, etc. Depuis le début, Dominique a fait le choix de vendre ses légumes sur les marchés afin de vous offrir des légumes frais savoureux et de qualité. Pour ceux qui ont envie de retrouver le plaisir de cultiver soi-même ses légumes, Dominique vend également des plants potagers. N’ayant pas la surface pour produire toute la gamme de légumes, Dominique s’approvisionne auprès des maraîchers bio des environs afin de vous proposer un étal le plus varié possible.",    
      "sourceKey":"Colibris", 
      "image" : "https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.italyguides.it%2Fimages%2Fgridfolio%2Ftaormina%2Ftaormina.02.jpg&f=1",
      "optionValues":[
        "10429",
        "10427"
      ]
    },
    {
      "id":"2PLG",
      "status":1,
      "moderationState":0,
      "name":"Philippe Le Gouard",
      "geo":{
        "latitude":47.8016,
        "longitude":-3.4808
      },
      "address":{
        "streetAddress":"<b>Addresse:</b> Coat er Malo",
        "addressLocality":"Guidel",
        "postalCode":"56620",
        "addressCountry":"FR"
      },
      "nom":"<b>Nom: </b> Philippe Le Gouard",
      "telephone":"<b>Telephone</b>: 06 47 07 61 35",
      "openHoursMoreInfos":"<b>Horaires:</b> De 11h à 18h le jeudi, vendredi et samedi",      
      "website":"<b>Lien</b>: -",   
      "Liste Produits":"<b>Liste Produits</b>: Radi, salade, carotte, oignon blanc, tomate, courgette, poivron, concombre, aubergine, mâche, épinard, navet",
      "avis":"<b>Avis: </b>"+ output +" \"Produits frais\" - Paul Dumas.",
      "modeAcheminement":"<b>Mode d'acheminement: </b> Vélo, voiture.",
      "conditionElevage":"<b>Condition d'elevage: </b> Exterieure.",
      "circuitProduction":"<b>Circuit de production: </b> Court, directe.",   
      "modeEmballageProduit":"<b>Mode emballage de produit: </b> Barquette .",
      "histoire":"<b>Histoire: </b> Après avoir été pendant longtemps commerçant ambulant de fruits et légumes sur les marchés, Philippe reprend en 1995 l’exploitation familiale. Il poursuit la culture maraîchère de saison, en essayant de traiter le moins possible.",    
      "sourceKey":"Colibris", 
      "image" : "https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.italyguides.it%2Fimages%2Fgridfolio%2Ftaormina%2Ftaormina.02.jpg&f=1",
      "optionValues":[
        "10429",
        "10427"
      ]
    },
  ]  
}
