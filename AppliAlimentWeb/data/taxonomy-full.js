var output = "<span class=\"fa fa-meat\"></span>";

var taxonomy = {
  "name":"Catégories Principales",
  "options":[
    {
      "id":10427,
      "name":"Légumes",
      "nameShort":"Légumes",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10428,
              "name":"Artichaut",
              "color":"#009A9C",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10429,
              "name":"Brocoli",
              "color":"#009A9C",
              "softColor":"#864C26",
              "icon":"icon-marche",
              "textHelper":"qui vendent des trucs bio"
            },
            {
              "id":10430,
              "name":"Carotte",
              "color":"#009A9C",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10431,
              "name":"Haricot vert",
              "color":"#009A9C",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10432,
              "name":"Laitue",
              "color":"#009A9C",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10433,
              "name":"Oignon",
              "color":"#009A9C",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10434,
              "name":"Pommme de terre",
              "color":"#009A9C",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10435,
              "name":"Tomate",
              "color":"#009A9C",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            }
          ],
          "unexpandable":true
        }
      ],
      "color":"#009A9C",
      "softColor":"#808700",
      "icon":"icon-legumes",
      "showOpenHours":true
    },
    {
      "id":10436,
      "name":"Fruits",
      "nameShort":"Fruits",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10437,
              "name":"Fraise",
              "color":"#ab7100",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10438,
              "name":"Framboise",
              "color":"#ab7100",
              "softColor":"#864C26",
              "icon":"icon-marche",
              "textHelper":"qui vendent des trucs bio"
            },
            {
              "id":10439,
              "name":"Kiwi",
              "color":"#ab7100",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10440,
              "name":"Poire",
              "color":"#ab7100",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10441,
              "name":"Pomme",
              "color":"#ab7100",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            }
          ],
          "unexpandable":true
        }
      ],
      "color":"#ab7100",
      "softColor":"#808700",
      "icon":"icon-fruits",
      "showOpenHours":true
    },
    {
      "id":10442,
      "name":"Produits Laitiers",
      "nameShort":"Produits Laitiers",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10443,
              "name":"Beure",
              "color":"#8e36a5",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10444,
              "name":"Frommage",
              "color":"#8e36a5",
              "softColor":"#864C26",
              "icon":"icon-marche",
              "textHelper":"qui vendent des trucs bio"
            },
            {
              "id":10445,
              "name":"Lait",
              "color":"#8e36a5",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10446,
              "name":"Yaourt",
              "color":"#8e36a5",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            }
          ],
          "unexpandable":true
        }
      ],
      "color":"#8e36a5",
      "softColor":"#808700",
      "icon":"icon-laitier",
      "showOpenHours":true
    },
    {
      "id":10447,
      "name":"Viande",
      "nameShort":"Viande",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10448,
              "name":"Agneau",
              "color":"#fc1c9c",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10449,
              "name":"Boeuf",
              "color":"#fc1c9c",
              "softColor":"#864C26",
              "icon":"icon-marche",
              "textHelper":"qui vendent des trucs bio"
            },
            {
              "id":10450,
              "name":"Porc",
              "color":"#fc1c9c",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10451,
              "name":"Veau",
              "color":"#fc1c9c",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            },
            {
              "id":10452,
              "name":"Volaille",
              "color":"#fc1c9c",
              "softColor":"#138C8E",
              "icon":"icon-marche",
              "textHelper":"Traiteurs mais pas restos"
            }
            
          ],
          "unexpandable":true
        }
      ],
      "color":"#fc1c9c",
      "softColor":"#808700",
      "icon":"icon-viande",
      "showOpenHours":true
    },
    {
      "id":10453,
      "name":"Miel",
      "nameShort":"Miel",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10454,
              "name":"Miel",
              "color":"#fccc3c",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            }         
          ],
          "unexpandable":true
        }
      ],
      "color":"#fccc3c",
      "softColor":"#808700",
      "icon":"icon-miel",
      "showOpenHours":true
    },
    {
      "id":10455,
      "name":"Oeufs",
      "nameShort":"Oeufs",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10456,
              "name":"Oeufs",
              "color":"#24b3fb",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            }         
          ],
          "unexpandable":true
        }
      ],
      "color":"#24b3fb",
      "softColor":"#808700",
      "icon":"icon-oeufs",
      "showOpenHours":true
    },
    {
      "id":10457,
      "name":"Produits de mer",
      "nameShort":"Produits de mer",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10458,
              "name":"Crustacés",
              "color":"#d9c352",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10459,
              "name":"Huitres",
              "color":"#d9c352",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10460,
              "name":"Poisson",
              "color":"#d9c352",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            } 
          ],
          "unexpandable":true
        }
      ],
      "color":"#d9c352",
      "softColor":"#808700",
      "icon":"icon-poisson",
      "showOpenHours":true
    },
    {
      "id":10461,
      "name":"Pain et Farine",
      "nameShort":"Farine",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10462,
              "name":"Farine",
              "color":"#c70000",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10463,
              "name":"Pain",
              "color":"#c70000",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            }
          ],
          "unexpandable":true
        }
      ],
      "color":"#c70000",
      "softColor":"#808700",
      "icon":"icon-pain",
      "showOpenHours":true
    },
    {
      "id":10464,
      "name":"Huiles et vinaigre",
      "nameShort":"Huiles et vinaigre",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10465,
              "name":"Huiles",
              "color":"#3e1313",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10466,
              "name":"Vinaigre",
              "color":"#3e1313",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            }
          ],
          "unexpandable":true
        }
      ],
      "color":"#3e1313",
      "softColor":"#808700",
      "icon":"icon-huile",
      "showOpenHours":true
    },
    {
      "id":10467,
      "name":"Boissons",
      "nameShort":"Boissons",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10468,
              "name":"Bières",
              "color":"#998f83",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10469,
              "name":"Jus",
              "color":"#998f83",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            }
          ],
          "unexpandable":true
        }
      ],
      "color":"#998f83",
      "softColor":"#808700",
      "icon":"icon-boissons",
      "showOpenHours":true
    },
    {
      "id":10470,
      "name":"Autres",
      "nameShort":"Autres",
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10471,
              "name":"Escargots",
              "color":"#f29657",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            },
            {
              "id":10472,
              "name":"Plantes",
              "color":"#f29657",
              "softColor":"#22698E",
              "icon":"icon-marche",
              "textHelper":"Marché paysans"
            }
          ],
          "unexpandable":true
        }
      ],
      "color":"#f29657",
      "softColor":"#808700",
      "icon":"icon-autre",
      "showOpenHours":true
    },
    {
      "id":10473,
      "name":"Jardins partagés",
      "nameShort":"Jardins partagés",
      "color":"#28b293",
      "softColor":"#808700",
      "icon":"icon-jaridnier",
      "showOpenHours":true
    }
  ],
  "unexpandable":true
}
